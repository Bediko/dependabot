# frozen_string_literal: true

describe Credentials do
  let(:github_token) { nil }

  let(:maven_url) { nil }
  let(:maven_username) { nil }
  let(:maven_password) { nil }

  let(:docker_registry) { nil }
  let(:docker_username) { nil }
  let(:docker_password) { nil }

  let(:npm_registry) { nil }
  let(:npm_token) { nil }

  let(:gitlab_creds) do
    {
      "type" => "git_source",
      "host" => URI(Settings.gitlab_url).host,
      "username" => "x-access-token",
      "password" => Settings.gitlab_access_token
    }
  end

  let(:github_creds) do
    {
      "type" => "git_source",
      "host" => "github.com",
      "username" => "x-access-token",
      "password" => Settings.github_access_token
    }
  end

  let(:maven_creds) do
    {
      "type" => "maven_repository",
      "url" => Settings.credentials.maven.repository.url,
      "username" => Settings.credentials.maven.repository.username,
      "password" => Settings.credentials.maven.repository.password
    }
  end

  let(:docker_creds) do
    {
      "type" => "docker_registry",
      "registry" => Settings.credentials.docker.registry.registry,
      "username" => Settings.credentials.docker.registry.username,
      "password" => Settings.credentials.docker.registry.password
    }
  end

  let(:npm_creds) do
    {
      "type" => "npm_registry",
      "registry" => Settings.credentials.npm.registry.registry,
      "token" => Settings.credentials.npm.registry.token
    }
  end

  subject { described_class.fetch }

  before do
    described_class.instance_variable_set(:@credentials, nil)

    Settings.add_source!(
      {
        github_access_token: github_token,
        credentials: {
          maven: {
            repository: {
              url: maven_url,
              username: maven_username,
              password: maven_password
            }
          },
          docker: {
            registry: {
              registry: docker_registry,
              username: docker_username,
              password: docker_password
            }
          },
          npm: {
            registry: {
              registry: npm_registry,
              token: npm_token
            }
          }
        }
      }
    )
    Settings.reload!
  end

  context "Gitlab credentials" do
    it "are configured" do
      expect(subject).to eq([gitlab_creds])
    end
  end

  context "Github credentials" do
    let(:github_token) { "token" }

    it "are configured" do
      expect(subject).to eq([github_creds, gitlab_creds])
    end
  end

  context "Maven credentials" do
    let(:maven_url) { "url" }
    let(:maven_username) { "username" }
    let(:maven_password) { "password" }

    it "are configured" do
      expect(subject).to eq([gitlab_creds, maven_creds])
    end
  end

  context "Docker credentials" do
    let(:docker_registry) { "dockerhub" }
    let(:docker_username) { "username" }
    let(:docker_password) { "password" }

    it "are configured" do
      expect(subject).to eq([gitlab_creds, docker_creds])
    end
  end

  context "Npm credentials" do
    let(:npm_registry) { "npm-private" }
    let(:npm_token) { "username" }

    it "are configured" do
      expect(subject).to eq([gitlab_creds, npm_creds])
    end
  end
end
